package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/nexus-cli/backup"
	"gitlab.com/logius/cloud-native-overheid/tools/nexus-cli/tenant"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "nexus",
		Short: "OPS tools for Nexus",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(tenant.NewCommand())
	cmd.AddCommand(backup.NewCommand())
}
