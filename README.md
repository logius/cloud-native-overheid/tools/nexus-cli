# Nexus-cli

## Configuration

```yaml
customer:
  name: sample

  nexus:
    use_maven: true
    use_npm: true
    use_nuget: true
```

- `customer.nexus.use_maven: true` Creates 2 Maven repo's: sample-snapshots and sample-releases
- `customer.nexus.use_npm: true` Creates 2 NPM repo's: sample-npm, sample-npm-group (containing sample-npm and registry.npmjs.org)
- `customer.nexus.use_nuget: true` Creates 2 Nuget repo's: sample-nuget, sample-nuget-group (containing sample-nuget and api.nuget.org)
- `customer.nexus.private_repos: true` keeps the repo's private
- `customer.nexus.public_and_private_repos: true` creates both public and private repo's


## private repos
We have enabled Anonymous Access on Nexus, so users may access repos as the 'Anonymous' user.
https://help.sonatype.com/repomanager3/system-configuration/user-authentication/anonymous-access
The Anonymous user is assigned read privileges for all repos, unless customers configure 'private_repos' as true.
If customers want both publicly accessible and private repos, they may set 'public_and_private_repos' as true.


## Building local image

Build image local:
```sh
./build/build.sh
```

Collect credentials for Nexus and Gitlab
Use the local build docker image to apply the customer.yaml:
```sh
NEXUS_URL=
NEXUS_USERNAME=
NEXUS_PASSWORD=
NEXUS_SYSTEM_ADMIN_GROUP=
NEXUS_CLI_IMAGE=registry.gitlab.com/logius/cloud-native-overheid/tools/nexus-cli:local

GITLAB_URL=
GITLAB_ACCESS_TOKEN=

LOCAL_CUSTOMER_CONFIG_FILE=$(pwd)/examples/customer.yaml
DEST_CUSTOMER_CONFIG_FILE=/customer.yaml

docker run -v $LOCAL_CUSTOMER_CONFIG_FILE:$DEST_CUSTOMER_CONFIG_FILE \
  -e NEXUS_USERNAME=$NEXUS_USERNAME \
  -e NEXUS_PASSWORD=$NEXUS_PASSWORD \
  -e NEXUS_URL=$NEXUS_URL
  -e NEXUS_SYSTEM_ADMIN_GROUP=$NEXUS_SYSTEM_ADMIN_GROUP
  -e GITLAB_ACCESS_TOKEN=$GITLAB_ACCESS_TOKEN \
  -e GITLAB_URL=$GITLAB_URL $NEXUS_CLI_IMAGE \
  configure-customer \
  --gitlab-url=${GITLAB_URL} \
  --nexus-url=${NEXUS_URL} \
  --nexus-username=${NEXUS_USERNAME} \
  --nexus-password=${NEXUS_PASSWORD} \
  --config=${DEST_CUSTOMER_CONFIG_FILE} \
  --system-admin-group=${NEXUS_SYSTEM_ADMIN_GROUP}
```
