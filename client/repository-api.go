package nexus

import (
	"fmt"
	"log"
	"strings"
	"time"
)

// ManageHostedMavenRepository creates or updates Hosted Maven Repository
func (nexusClient Client) ManageHostedMavenRepository(repositoryName string, versionPolicy string, cleanupPolicy string, strictContentTypeValidation bool) error {

	type Storage struct {
		WritePolicy                 string `json:"writePolicy"`
		BlobStoreName               string `json:"blobStoreName"`
		StrictContentTypeValidation bool   `json:"strictContentTypeValidation"`
	}
	type Maven struct {
		VersionPolicy string `json:"versionPolicy"`
		LayoutPolicy  string `json:"layoutPolicy"`
	}
	type Cleanup struct {
		PolicyNames []string `json:"policyNames,omitempty"`
	}
	type Repository struct {
		Name    string   `json:"name"`
		Online  bool     `json:"online"`
		Storage Storage  `json:"storage"`
		Maven   Maven    `json:"maven"`
		Cleanup *Cleanup `json:"cleanup,omitempty"`
	}

	statusCode, err := nexusClient.doGetRequest("v1/repositories/maven/hosted/"+repositoryName, nil)
	if err != nil {
		return err
	}

	repository := &Repository{
		Name:   repositoryName,
		Online: true,
		Storage: Storage{
			WritePolicy:                 "Allow",
			BlobStoreName:               "default",
			StrictContentTypeValidation: strictContentTypeValidation,
		},
		Maven: Maven{
			VersionPolicy: versionPolicy,
			LayoutPolicy:  "Strict",
		},
	}
	if cleanupPolicy != "" {
		repository.Cleanup = &Cleanup{
			PolicyNames: []string{cleanupPolicy},
		}
	}

	if statusCode == 200 {
		log.Printf("Update Hosted Maven Repository %q", repositoryName)
		if err := nexusClient.doPutRequest("v1/repositories/maven/hosted/"+repositoryName, repository); err != nil {
			return err
		}
	} else {
		log.Printf("Create Hosted Maven Repository %q", repositoryName)
		if err := nexusClient.doPostRequest("v1/repositories/maven/hosted", repository, nil); err != nil {
			return err
		}
	}

	return nil
}

// CreateProxyMavenRepository creates Proxy Maven Repository for a customer
func (nexusClient Client) CreateProxyMavenRepository(customerName string, proxyName string, proxyUrl string, versionPolicy string, cleanupPolicy string, strictContentTypeValidation bool) error {

	repositoryName := customerName + "-" + proxyName

	statusCode, err := nexusClient.doGetRequest("v1/repositories/maven/proxy/"+repositoryName, nil)
	if err != nil {
		return err
	}

	repoBodyTemplate := `{
		"name": "%s",
		"online": true,
		"storage": {
			"blobStoreName": "default",
			"strictContentTypeValidation": %t
		},
		"cleanup": {
			"policyNames": ["%s"]
		},
		"proxy": {
			"remoteUrl": "%s",
			"contentMaxAge": -1,
			"metadataMaxAge": 480
		},
		"negativeCache": {
			"enabled": true,
			"timeToLive": 5
		},
		"httpClient": {
			"blocked": false,
			"autoBlock": true,
			"connection": {
				"retries": 0,
				"userAgentSuffix": "string",
				"timeout": 60,
				"enableCircularRedirects": false,
				"enableCookies": false,
				"useTrustStore": false
			}
		},
		"maven": {
			"versionPolicy": "%s",
			"layoutPolicy": "STRICT"
		}
	}`
	repoBody := fmt.Sprintf(repoBodyTemplate, repositoryName, strictContentTypeValidation, cleanupPolicy, proxyUrl, versionPolicy)

	if statusCode == 200 {
		log.Printf("Update Proxy Maven Repository %q", repositoryName)
		if err := nexusClient.doPutRequest("v1/repositories/maven/proxy/"+repositoryName, repoBody); err != nil {
			return err
		}
	} else {
		log.Printf("Create Proxy Maven Repository %q", repositoryName)
		if err := nexusClient.doPostRequest("v1/repositories/maven/proxy", repoBody, nil); err != nil {
			return err
		}
	}
	return nil

}

// CreateHostedNpmRepository creates hosted NPM repository with the given repositoryName
func (nexusClient Client) CreateHostedNpmRepository(repositoryName string, strictContentTypeValidation bool) error {

	// Does it already exist?
	statusCode, err := nexusClient.doGetRequest("v1/repositories/npm/hosted/"+repositoryName, nil)
	if err != nil {
		return err
	}

	repoBodyTemplate := `{
		"name": "%s",
		"online": true,
		"storage": {
			"blobStoreName": "default",
			"strictContentTypeValidation": %t,
			"writePolicy": "allow_once"
		},
		"cleanup": {
			"policyNames": []
		},
		"component": {
			"proprietaryComponents": true
		}
	}`
	repoBody := fmt.Sprintf(repoBodyTemplate, repositoryName, strictContentTypeValidation)

	if statusCode == 200 {
		log.Printf("Update Hosted Npm Repository %q", repositoryName)
		if err := nexusClient.doPutRequest("v1/repositories/npm/hosted/"+repositoryName, repoBody); err != nil {
			return err
		}
	} else {
		log.Printf("Create Hosted Npm Repository %q", repositoryName)
		if err := nexusClient.doPostRequest("v1/repositories/npm/hosted", repoBody, nil); err != nil {
			return err
		}
	}
	return nil
}

// CreateProxyNpmRepository creates proxy NPM repository
func (nexusClient Client) CreateProxyNpmRepository(repositoryName string, url string, strictContentTypeValidation bool) error {

	// Does it already exist?
	statusCode, err := nexusClient.doGetRequest("v1/repositories/npm/proxy/"+repositoryName, nil)
	if err != nil {
		return err
	}

	newRepoTemplate := `{
		"name": "%s",
		"online": true,
		"storage": {
			"blobStoreName": "default",
			"strictContentTypeValidation": %t
		},
		"cleanup": {
			"policyNames": []
		},
		"proxy": {
			"remoteUrl": "%s",
			"contentMaxAge": -1,
			"metadataMaxAge": 480
		},
		"negativeCache": {
			"enabled": true,
			"timeToLive": 5
		},
		"httpClient": {
			"blocked": false,
			"autoBlock": false,
			"connection": null,
			"authentication": null
		}
	}`

	repoBody := fmt.Sprintf(newRepoTemplate, repositoryName, strictContentTypeValidation, url)

	if statusCode == 200 {
		log.Printf("Update Proxy Npm Repository %v for %v", repositoryName, url)
		if err := nexusClient.doPutRequest("v1/repositories/npm/proxy/"+repositoryName, repoBody); err != nil {
			return err
		}
	} else {
		log.Printf("Create Proxy Npm Repository %v for %v", repositoryName, url)
		if err := nexusClient.doPostRequest("v1/repositories/npm/proxy", repoBody, nil); err != nil {
			return err
		}
	}
	return nil
}

// CreateGroupNpmRepository creates group NPM repository
func (nexusClient Client) CreateGroupNpmRepository(repositoryName string, reposInGroup []string, strictContentTypeValidation bool) error {

	// Does it already exist?
	statusCode, err := nexusClient.doGetRequest("v1/repositories/npm/group/"+repositoryName, nil)
	if err != nil {
		return err
	}

	newRepoTemplate := `{
		"name": "%s",
		"online": true,
		"storage": {
			"blobStoreName": "default",
			"strictContentTypeValidation": %t
		},
		"group": {
			"memberNames": [
			  %s
			],
			"writableMember": null
		}
	}`

	repoBody := fmt.Sprintf(newRepoTemplate, repositoryName, strictContentTypeValidation, joinNamesArray(reposInGroup))

	if statusCode == 200 {
		log.Printf("Update Group Npm Repository %v for repo's %+v", repositoryName, reposInGroup)
		if err := nexusClient.doPutRequest("v1/repositories/npm/group/"+repositoryName, repoBody); err != nil {
			return err
		}
	} else {
		log.Printf("Create Group Npm Repository %v for repo's %+v", repositoryName, reposInGroup)
		if err := nexusClient.doPostRequest("v1/repositories/npm/group", repoBody, nil); err != nil {
			return err
		}
	}
	return nil
}

// Nuget

// CreateHostedNugetRepository creates hosted Nuget repository with the given repositoryName
func (nexusClient Client) CreateHostedNugetRepository(repositoryName string, strictContentTypeValidation bool) error {

	// Does it already exist?
	statusCode, err := nexusClient.doGetRequest("v1/repositories/nuget/hosted/"+repositoryName, nil)
	if err != nil {
		return err
	}

	repoBodyTemplate := `{
		"name": "%s",
		"online": true,
		"storage": {
			"blobStoreName": "default",
			"strictContentTypeValidation": %t,
			"writePolicy": "ALLOW"
		},
		"cleanup": {
			"policyNames": []
		},
		"component": {
			"proprietaryComponents": true
		}
	}`
	repoBody := fmt.Sprintf(repoBodyTemplate, repositoryName, strictContentTypeValidation)

	if statusCode == 200 {
		log.Printf("Update Hosted Nuget Repository %q", repositoryName)
		if err := nexusClient.doPutRequest("v1/repositories/nuget/hosted/"+repositoryName, repoBody); err != nil {
			return err
		}
	} else {
		log.Printf("Create Hosted Nuget Repository %q", repositoryName)
		if err := nexusClient.doPostRequest("v1/repositories/nuget/hosted", repoBody, nil); err != nil {
			return err
		}
	}
	return nil
}

// CreateProxyNugetRepository creates proxy Nuget repository
func (nexusClient Client) CreateProxyNugetRepository(repositoryName string, url string, strictContentTypeValidation bool) error {

	// Does it already exist?
	statusCode, err := nexusClient.doGetRequest("v1/repositories/nuget/proxy/"+repositoryName, nil)
	if err != nil {
		return err
	}

	newRepoTemplate := `{
		"name": "%s",
		"online": true,
		"storage": {
			"blobStoreName": "default",
			"strictContentTypeValidation": %t
		},
		"cleanup": {
			"policyNames": []
		},
		"proxy": {
			"remoteUrl": "%s",
			"contentMaxAge": -1,
			"metadataMaxAge": 480
		},
		"negativeCache": {
			"enabled": true,
			"timeToLive": 5
		},
		"httpClient": {
			"blocked": false,
			"autoBlock": false,
			"connection": null,
			"authentication": null
		},
		"nugetProxy": {
			"queryCacheItemMaxAge": 3600,
			"nugetVersion": "V3"
		}
	}`

	repoBody := fmt.Sprintf(newRepoTemplate, repositoryName, strictContentTypeValidation, url)

	if statusCode == 200 {
		log.Printf("Update Proxy Nuget Repository %v for %v", repositoryName, url)
		if err := nexusClient.doPutRequest("v1/repositories/nuget/proxy/"+repositoryName, repoBody); err != nil {
			return err
		}
	} else {
		log.Printf("Create Proxy Nuget Repository %v for %v", repositoryName, url)
		if err := nexusClient.doPostRequest("v1/repositories/nuget/proxy", repoBody, nil); err != nil {
			return err
		}
	}
	return nil
}

// CreateGroupNugetRepository creates group Nuget repository
func (nexusClient Client) CreateGroupNugetRepository(repositoryName string, reposInGroup []string, strictContentTypeValidation bool) error {

	// Does it already exist?
	statusCode, err := nexusClient.doGetRequest("v1/repositories/nuget/group/"+repositoryName, nil)
	if err != nil {
		return err
	}

	newRepoTemplate := `{
		"name": "%s",
		"online": true,
		"storage": {
			"blobStoreName": "default",
			"strictContentTypeValidation": %t
		},
		"group": {
			"memberNames": [
			  %s
			]
		}
	}`

	repoBody := fmt.Sprintf(newRepoTemplate, repositoryName, strictContentTypeValidation, joinNamesArray(reposInGroup))

	if statusCode == 200 {
		log.Printf("Update Group Nuget Repository %v for repo's %+v", repositoryName, reposInGroup)
		if err := nexusClient.doPutRequest("v1/repositories/nuget/group/"+repositoryName, repoBody); err != nil {
			return err
		}
	} else {
		log.Printf("Create Group Nuget Repository %v for repo's %+v", repositoryName, reposInGroup)
		if err := nexusClient.doPostRequest("v1/repositories/nuget/group", repoBody, nil); err != nil {
			return err
		}
	}
	return nil
}

// joinNamesArray joins the strings in the given slice, quoted and separated by a comma
func joinNamesArray(names []string) string {
	return `"` + strings.Join(names, `","`) + `"`
}

func (nexusClient Client) GetHostedMavenReleaseRepositories() ([]string, error) {

	type Repository struct {
		Name   string
		Format string
		Type   string
	}

	var repositories []Repository
	log.Printf("Get Hosted Maven Release Repositories")
	_, err := nexusClient.doGetRequest("v1/repositories", &repositories)

	if err != nil {
		return nil, err
	}
	repoNames := make([]string, 0)
	for _, repository := range repositories {
		if repository.Format == "maven2" && repository.Type == "hosted" && !strings.HasSuffix(repository.Name, "snapshots") {
			repoNames = append(repoNames, repository.Name)
		}
	}
	return repoNames, nil
}

type Asset struct {
	BlobCreated time.Time `json:"blobCreated"`
	Checksum    struct {
		Md5  string `json:"md5"`
		Sha1 string `json:"sha1"`
	} `json:"checksum"`
	ContentType string `json:"contentType"`
	DownloadURL string `json:"downloadUrl"`
	Format      string `json:"format"`
	ID          string `json:"id"`
	Maven2      struct {
		ArtifactID string `json:"artifactId"`
		Extension  string `json:"extension"`
		GroupID    string `json:"groupId"`
		Version    string `json:"version"`
	} `json:"maven2"`
	Path       string `json:"path"`
	Repository string `json:"repository"`
}

func (nexusClient Client) GetMavenAssets(repository string) ([]Asset, error) {

	assets := make([]Asset, 0)
	type AssetList struct {
		Items             []Asset `json:"items"`
		ContinuationToken string  `json:"continuationToken"`
	}

	log.Printf("Get Assets for repository %q", repository)

	// The query supports pagination with the query paramater continuationToken
	for continuationToken := ""; ; {

		url := "v1/assets?repository=" + repository
		if continuationToken != "" {
			url += "&continuationToken=" + continuationToken
		}

		var assetList AssetList
		_, err := nexusClient.doGetRequest(url, &assetList)
		if err != nil {
			return nil, err
		}

		for _, asset := range assetList.Items {
			if isValidAsset(&asset) {
				assets = append(assets, asset)
			}
		}

		if continuationToken = assetList.ContinuationToken; continuationToken == "" {
			break
		}
	}

	return assets, nil
}

func isValidAsset(asset *Asset) bool {
	extension := asset.Maven2.Extension
	if extension == "" {
		return false
	}
	if strings.HasSuffix(extension, "md5") || strings.HasSuffix(extension, "sha1") {
		return false
	}
	if strings.HasSuffix(extension, "sha512") || strings.HasSuffix(extension, "sha256") {
		return false
	}
	return true
}
