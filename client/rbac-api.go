package nexus

import (
	"log"
	"net/url"
	"strings"

	"gitlab.com/logius/cloud-native-overheid/tools/nexus-cli/config"
)

// Role represents a Nexus Role
type Role struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Privileges  []string `json:"privileges"`
	Roles       []string `json:"roles"`
}

// CreateDeployerRole creates a Nexus Deployer role
func (nexusClient Client) CreateDeployerRole(customer config.Customer) error {

	roleID := customer.Name + "_deployer"

	statusCode, err := nexusClient.doGetRequest("v1/security/roles/"+roleID, nil)
	if err != nil {
		return err
	}

	role := basicRole(roleID, customer.Name, customer.Nexus)
	role.Name = roleID
	role.Description = customer.Name + " deployer role"
	if statusCode == 200 {
		log.Printf("Update Deployer Role with path: %s", "v1/security/roles/"+url.QueryEscape(roleID))
		if err := nexusClient.doPutRequest("v1/security/roles/"+url.QueryEscape(roleID), role); err != nil {
			return err
		}
	} else {
		log.Printf("Create Deployer Role %q", role.ID)
		if err := nexusClient.doPostRequest("v1/security/roles", role, nil); err != nil {
			return err
		}
	}
	return nil
}

// CreateAdminRole creates a Nexus role for an administrator
func (nexusClient Client) CreateAdminRole(customer config.Customer, groupName string, rolePrefix string) error {
	roleID := rolePrefix + groupName

	statusCode, err := nexusClient.doGetRequest("v1/security/roles/"+url.QueryEscape(roleID), nil)
	if err != nil {
		return err
	}

	role := basicRole(roleID, customer.Name, customer.Nexus)
	role.Name = groupName
	role.Description = groupName + " role"

	if statusCode == 200 {
		log.Printf("Update Admin Role with path: %s", "v1/security/roles/"+url.QueryEscape(roleID))
		if err := nexusClient.doPutRequest("v1/security/roles/"+url.QueryEscape(roleID), role); err != nil {
			return err
		}
	} else if statusCode == 404 {
		log.Printf("Create Admin Role %q", roleID)
		if err := nexusClient.doPostRequest("v1/security/roles", role, nil); err != nil {
			return err
		}
	}

	return nil
}

func basicRole(roleID, customerName string, customerNexusConfig config.Nexus) *Role {

	privileges := []string{"nx-component-upload", "nx-healthcheck-read", "nx-search-read"}

	if customerNexusConfig.UseMaven {
		privileges = append(privileges,
			"nx-repository-admin-maven2-"+customerName+"-releases-*",
			"nx-repository-view-maven2-"+customerName+"-releases-*",
			"nx-repository-admin-maven2-"+customerName+"-snapshots-*",
			"nx-repository-view-maven2-"+customerName+"-snapshots-*",
			"nx-repository-view-maven2-maven-central-*")
	}

	if customerNexusConfig.UseNpm {
		privileges = append(privileges,
			"nx-repository-admin-npm-"+customerName+"-npm-*",
			"nx-repository-view-npm-"+customerName+"-npm-*",
			"nx-repository-admin-npm-"+customerName+"-npm-group-*",
			"nx-repository-view-npm-"+customerName+"-npm-group-*",
			"nx-repository-view-npm-registry.npmjs.org-*")
	}

	if customerNexusConfig.UseNuget {
		privileges = append(privileges,
			"nx-repository-admin-nuget-"+customerName+"-nuget-*",
			"nx-repository-view-nuget-"+customerName+"-nuget-*",
			"nx-repository-admin-nuget-"+customerName+"-nuget-group-*",
			"nx-repository-view-nuget-"+customerName+"-nuget-group-*",
			"nx-repository-view-nuget-api.nuget.org-*",
			"nx-apikey-all")
	}

	if customerNexusConfig.PublicAndPrivateRepo {
		privileges = append(privileges,
			"nx-repository-admin-maven2-"+customerName+"-releases-private-*",
			"nx-repository-view-maven2-"+customerName+"-releases-private-*",
			"nx-repository-admin-maven2-"+customerName+"-snapshots-private-*",
			"nx-repository-view-maven2-"+customerName+"-snapshots-private-*",
			"nx-repository-admin-npm-"+customerName+"-npm-private-*",
			"nx-repository-view-npm-"+customerName+"-npm-private-*")
	}

	if len(customerNexusConfig.M2ProxyRepositories) > 0 {
		for _, m2ProxyRepository := range customerNexusConfig.M2ProxyRepositories {
			privileges = append(privileges,
				"nx-repository-admin-maven2-"+customerName+"-"+m2ProxyRepository.Name+"-*",
				"nx-repository-view-maven2-"+customerName+"-"+m2ProxyRepository.Name+"-*")
		}
	}

	return &Role{
		ID:         roleID,
		Privileges: privileges,
	}
}

type User struct {
	UserID       string   `json:"userId"`
	Password     string   `json:"password"`
	FirstName    string   `json:"firstName"`
	LastName     string   `json:"lastName"`
	EmailAddress string   `json:"emailAddress"`
	Status       string   `json:"status"`
	Roles        []string `json:"roles"`
	Source       string   `json:"source"`
}

// CreateDeployerUser creates a Nexus deployer user
func (nexusClient Client) CreateDeployerUser(customerName string, deployerUser string, password string, publicRoleName string) error {

	var users []User
	statusCode, err := nexusClient.doGetRequest("v1/security/users?source=default&userId="+deployerUser, &users)
	if err != nil {
		return err
	}
	if statusCode == 200 && len(users) > 0 {
		// delete the user if it does not have the correct role (the user will be created)
		if !userHasRole(users[0], deployerUser) {
			if err := nexusClient.doDeleteRequest("v1/security/users/" + deployerUser); err != nil {
				return err
			}
		} else {
			log.Printf("Update password for deployer user %q", deployerUser)
			err := nexusClient.doPutRequest("v1/security/users/"+deployerUser+"/change-password", password)
			if err != nil {
				return err
			}
			return nil // nothing else to do
		}
	}

	user := User{
		UserID:       deployerUser,
		Password:     password,
		FirstName:    customerName,
		LastName:     "Deployer",
		EmailAddress: "deployer@noreply.nl",
		Status:       "active",
		Roles: []string{
			deployerUser,
			publicRoleName,
		},
	}
	log.Printf("Create deployer user %q", deployerUser)
	return nexusClient.doPostRequest("v1/security/users", &user, nil)
}

func userHasRole(user User, role string) bool {
	for _, userRole := range user.Roles {
		if userRole == role {
			return true
		}
	}
	return false
}

// Public role provides access to all public repositories
func (nexusClient Client) CreatePublicRole(publicRoleName string) error {

	statusCode, err := nexusClient.doGetRequest("v1/security/roles/"+publicRoleName, nil)
	if err != nil {
		return err
	}

	if statusCode == 200 {
		log.Printf("confirmed presence of %q role", publicRoleName)
	} else {

		role := &Role{
			ID:          publicRoleName,
			Name:        publicRoleName,
			Description: "provides access to all public repositories",
			Privileges: []string{
				"nx-repository-view-maven2-maven-central-*",
				"nx-repository-view-npm-registry.npmjs.org-*",
				"nx-repository-view-maven2-maven-public-*",
				"nx-repository-view-nuget-api.nuget.org-*",
			},
		}

		log.Printf("Create Role %q", publicRoleName)
		if err := nexusClient.doPostRequest("v1/security/roles", role, nil); err != nil {
			return err
		}
	}

	// alter anonymous access user to use public role
	if err := nexusClient.anonymousUser(publicRoleName); err != nil {
		return err
	}

	return nil
}

func (nexusClient Client) anonymousUser(publicRoleName string) error {
	user := User{
		UserID:       "anonymous",
		FirstName:    "Anonymous",
		LastName:     "User",
		EmailAddress: "anonymous@example.org",
		Status:       "active",
		Roles:        []string{publicRoleName},
		Source:       "default",
	}

	if err := nexusClient.doPutRequest("v1/security/users/anonymous", user); err != nil {
		return err
	}

	log.Printf("Updated anonymous user with role %q", publicRoleName)
	return nil

}

// append public role with read and search privileges for customer
func (nexusClient Client) AppendPublicRole(customerName string, publicRoleName string, useMaven bool, useNpm bool, useNuget bool) error {

	privileges := nexusClient.getPrivileges(publicRoleName)

	if useMaven {
		privileges = append(privileges,
			"nx-repository-view-maven2-"+customerName+"-releases-read",
			"nx-repository-view-maven2-"+customerName+"-releases-browse",
			"nx-repository-view-maven2-"+customerName+"-snapshots-read",
			"nx-repository-view-maven2-"+customerName+"-snapshots-browse")
	}

	if useNpm {
		privileges = append(privileges,
			"nx-repository-view-npm-"+customerName+"-npm-browse",
			"nx-repository-view-npm-"+customerName+"-npm-read",
			"nx-repository-view-npm-"+customerName+"-npm-group-browse",
			"nx-repository-view-npm-"+customerName+"-npm-group-read")
	}

	if useNuget {
		privileges = append(privileges,
			"nx-repository-view-nuget-"+customerName+"-nuget-browse",
			"nx-repository-view-nuget-"+customerName+"-nuget-read",
			"nx-repository-view-nuget-"+customerName+"-nuget-group-browse",
			"nx-repository-view-nuget-"+customerName+"-nuget-group-read")
	}

	role := &Role{
		ID:          publicRoleName,
		Name:        publicRoleName,
		Description: "provides access to all public repositories",
		Privileges:  privileges,
	}

	statusCode, err := nexusClient.doGetRequest("v1/security/roles/"+url.QueryEscape(publicRoleName), nil)
	if err != nil {
		return err
	}

	if statusCode == 200 {
		log.Printf("Update Public Role with path: %s", "v1/security/roles/"+url.QueryEscape(publicRoleName))
		if err := nexusClient.doPutRequest("v1/security/roles/"+url.QueryEscape(publicRoleName), role); err != nil {
			return err
		}
	} else if statusCode == 404 {
		log.Printf("Create Public Role %q", publicRoleName)
		if err := nexusClient.doPostRequest("v1/security/roles", role, nil); err != nil {
			return err
		}
	}

	return nil
}

func (nexusClient Client) getPrivileges(publicRoleName string) (privileges []string) {

	var publicRole *Role

	statusCode, _ := nexusClient.doGetRequest("v1/security/roles/"+publicRoleName, &publicRole)
	if statusCode == 200 {
		return publicRole.Privileges
	} else {
		return nil
	}
}

func (nexusClient Client) RemoveFromPublicRole(customerName string, publicRoleName string) error {

	var privileges []string

	p := nexusClient.getPrivileges(publicRoleName)

	for _, v := range p {
		if !strings.Contains(v, "-"+customerName+"-") {
			privileges = append(privileges, v)
		}
	}

	role := &Role{
		ID:          publicRoleName,
		Name:        publicRoleName,
		Description: "provides access to all public repositories",
		Privileges:  privileges,
	}

	log.Printf("Remove private repos from Public Role with path: %s", "v1/security/roles/"+url.QueryEscape(publicRoleName))
	if err := nexusClient.doPutRequest("v1/security/roles/"+url.QueryEscape(publicRoleName), role); err != nil {
		return err
	}
	return nil

}
