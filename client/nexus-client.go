package nexus

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
	"strings"

	"github.com/pkg/errors"
)

// Content types
const (
	jsonContentType = "application/json"
)

// Client struct with api methods and accesstoken
type Client struct {
	NexusURL   string
	Username   string
	Password   string
	PathPrefix string
}

// NewNexusClient initalizes a new Client struct and connects to Nexus
// There is an SDK: github.com/sonatype-nexus-community/gonexus, but v0.59.0 has insufficient support for managing roles and repo's.
func NewNexusClient(nexusURL string, username string, password string, pathPrefix string) (*Client, error) {
	nexusClient := &Client{
		NexusURL:   nexusURL,
		Username:   username,
		Password:   password,
		PathPrefix: pathPrefix,
	}

	if nexusClient.Username == "" {
		return nil, errors.Errorf("Missing environment variable NEXUS_USERNAME")
	}
	if nexusClient.Password == "" {
		return nil, errors.Errorf("Missing environment variable NEXUS_PASSWORD")
	}

	if _, err := nexusClient.doGetRequest("v1/status", nil); err != nil {
		return nil, err
	}
	log.Printf("Nexus is available at %q", nexusClient.NexusURL)
	log.Printf("api endpoint is available at %q", nexusClient.PathPrefix)

	return nexusClient, nil
}

func (nexusClient Client) doGetRequest(requestPath string, response interface{}) (int, error) {
	return nexusClient.doJSONRequest(http.MethodGet, requestPath, nil, response, []int{200, 404})
}

func (nexusClient Client) doDeleteRequest(requestPath string) error {
	if _, err := nexusClient.doJSONRequest(http.MethodDelete, requestPath, nil, nil, []int{204}); err != nil {
		return err
	}
	return nil
}

func (nexusClient Client) doPutRequest(requestPath string, body interface{}) error {
	if _, err := nexusClient.doJSONRequest(http.MethodPut, requestPath, body, nil, []int{204}); err != nil {
		return err
	}
	return nil
}

func (nexusClient Client) doPostRequest(requestPath string, body interface{}, response interface{}) error {
	_, err := nexusClient.doJSONRequest(http.MethodPost, requestPath, body, response, []int{200, 201})
	if err != nil {
		return err
	}
	return nil
}

func (nexusClient Client) doJSONRequest(method string, requestPath string, body interface{}, response interface{}, allowedStatuscodes []int) (int, error) {

	url := fmt.Sprintf("%s/%s/rest/%s", nexusClient.NexusURL, nexusClient.PathPrefix, requestPath)

	var req *http.Request
	var err error

	// using json encode will add double quotes around body value => stringReader for strings
	if body != nil && reflect.TypeOf(body).String() == "string" {
		stringReader := strings.NewReader(body.(string))
		req, err = http.NewRequest(method, url, stringReader)
		if strings.HasPrefix(fmt.Sprintf("%v", body), "{") {
			req.Header.Set("Content-Type", "application/json")
		} else {
			req.Header.Set("Content-Type", "text/plain")
		}
	} else {
		buf := new(bytes.Buffer)
		json.NewEncoder(buf).Encode(body)
		req, err = http.NewRequest(method, url, buf)
		req.Header.Set("Content-Type", jsonContentType)
	}
	if err != nil {
		return 0, err
	}

	req.SetBasicAuth(nexusClient.Username, nexusClient.Password)

	var client = &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return 0, err
	}
	defer res.Body.Close()

	if !statusCodeOK(res.StatusCode, allowedStatuscodes) {
		return 0, errors.Errorf("URL: %s, Method %s, Body: %s, Error status code: %d", url, method, body, res.StatusCode)
	}

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return 0, err
	}

	//log.Printf("%v", string(responseData))
	if len(responseData) > 0 && response != nil {
		err = json.Unmarshal(responseData, response)
		if err != nil {
			return 0, errors.Errorf("URL: %s, Method %s, Body: %s, Response: %s", url, method, body, responseData)
		}
	}

	return res.StatusCode, nil
}

func statusCodeOK(statusCode int, allowedStatuscodes []int) bool {
	for _, code := range allowedStatuscodes {
		if code == statusCode {
			return true
		}
	}
	return false
}
