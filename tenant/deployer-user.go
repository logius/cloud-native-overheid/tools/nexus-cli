package tenant

import (
	"fmt"
	"log"
	"os"

	"github.com/sethvargo/go-password/password"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
	nexus "gitlab.com/logius/cloud-native-overheid/tools/nexus-cli/client"
	"gitlab.com/logius/cloud-native-overheid/tools/nexus-cli/config"
)

const NEXUS_ROBOT_SECRET = "NEXUS_ROBOT_SECRET"
const NEXUS_ROBOT_NAME = "NEXUS_ROBOT_NAME"

func createDeployerRole(nexusClient *nexus.Client, publicRoleName string, customerConfig *config.Customer) error {
	if err := nexusClient.CreateDeployerRole(*customerConfig); err != nil {
		return err
	}

	if customerConfig.Nexus.PrivateRepo != true || customerConfig.Nexus.PublicAndPrivateRepo == true {
		// append public repo's to public role
		if err := nexusClient.AppendPublicRole(customerConfig.Name, publicRoleName, customerConfig.Nexus.UseMaven, customerConfig.Nexus.UseNpm, customerConfig.Nexus.UseNuget); err != nil {
			return err
		}
	}

	if customerConfig.Nexus.PrivateRepo == true && customerConfig.Nexus.PublicAndPrivateRepo != true {
		// remove public repo's from public role
		if err := nexusClient.RemoveFromPublicRole(customerConfig.Name, publicRoleName); err != nil {
			return err
		}
	}
	return nil
}

func createDeployerUserWithExistingPassword(nexusClient *nexus.Client, gitlabURL string, publicRoleName string, customerConfig *config.Customer) error {

	deployerUser := customerConfig.Name + "_deployer"

	gitLabClient := gitlabclient.NewGitLabClient(gitlabURL)

	for _, gitlabGroup := range customerConfig.GitLab.Groups {
		deployerPassword, err := getGroupVarValue(gitLabClient, gitlabGroup.Name, NEXUS_ROBOT_SECRET)
		if err != nil {
			return err
		}
		if err := nexusClient.CreateDeployerUser(customerConfig.Name, deployerUser, deployerPassword, publicRoleName); err != nil {
			return err
		}
	}

	return nil
}

func createDeployerUserWithNewPassword(nexusClient *nexus.Client, gitlabURL string, gitlabURLPeer string, publicRoleName string, customerConfig *config.Customer) error {

	deployerUser := customerConfig.Name + "_deployer"

	gitLabClient := gitlabclient.NewGitLabClient(gitlabURL)

	var gitlabClientPeer *gitlab.Client
	if gitlabURLPeer != "" {
		gitlabClientPeer, _ = gitlab.NewClient(os.Getenv("GITLAB_ACCESS_TOKEN"), gitlab.WithBaseURL(fmt.Sprintf("%s/api/v4", gitlabURLPeer)))
	}

	passgenerator, err := password.NewGenerator(&password.GeneratorInput{
		// List of symbols that may be used.
		Symbols: "~@#()_-=|[]:<>",
	})
	if err != nil {
		return err
	}

	// First character should not be a symbol
	deployerPasswordLast19, err := passgenerator.Generate(19, 5, 5, false, false)
	if err != nil {
		return err
	}
	deployerPasswordFirstChar, err := passgenerator.Generate(1, 0, 0, false, false)
	if err != nil {
		return err
	}
	deployerPassword := deployerPasswordFirstChar + deployerPasswordLast19

	if err := nexusClient.CreateDeployerUser(customerConfig.Name, deployerUser, deployerPassword, publicRoleName); err != nil {
		return err
	}

	groupVars := make(map[string]gitlabclient.GroupVariable)
	groupVars[NEXUS_ROBOT_NAME] = gitlabclient.GroupVariable{
		Value:  deployerUser,
		Masked: false,
	}
	groupVars[NEXUS_ROBOT_SECRET] = gitlabclient.GroupVariable{
		Value:  deployerPassword,
		Masked: false,
	}

	for _, gitlabGroup := range customerConfig.GitLab.Groups {

		if err := gitlabclient.UpdateVarsInGitLabGroup(gitLabClient, gitlabGroup.Name, groupVars); err != nil {
			return err
		}

		if gitlabClientPeer != nil {
			err := gitlabclient.UpdateVarsInGitLabGroup(gitlabClientPeer, gitlabGroup.Name, groupVars)
			if err != nil {
				log.Printf("Ignoring error updating gitlab variables for the DR site. err= %v", err)
			}
		}
	}
	return nil
}

func getGroupVarValue(gitlabClient *gitlab.Client, groupPath string, variable string) (string, error) {
	group, err := gitlabclient.GetGitLabGroup(gitlabClient, groupPath)
	if err != nil {
		return "", err
	}

	if group == nil {
		return "", fmt.Errorf("could not find group %s", groupPath)
	}

	groupVarExists, _, _ := gitlabClient.GroupVariables.GetVariable(group.ID, variable)
	return groupVarExists.Value, nil
}
