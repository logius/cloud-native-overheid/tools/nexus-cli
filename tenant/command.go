package tenant

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	nexus "gitlab.com/logius/cloud-native-overheid/tools/nexus-cli/client"
	"gitlab.com/logius/cloud-native-overheid/tools/nexus-cli/config"
)

type flags struct {
	config                     *string
	publicRoleName             *string
	nexusURL                   *string
	pathPrefix                 *string
	nexusUsername              *string
	nexusPassword              *string
	gitlabURL                  *string
	gitlabURLPeer              *string
	systemAdminGroup           *string
	mavenSnapshotCleanupPolicy *string
	rolePrefix                 *string
	useExistingRobotPassword   *bool
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := configureTenant(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "configure-customer",
		Short: "Configure Nexus tenant",
		Long:  "This command configures repository and RBAC in Nexus.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.publicRoleName = cmd.Flags().String("publicRoleName", "", "name of Nexus rbac role for access to public repos")
	flags.nexusURL = cmd.Flags().String("nexus-url", "", "Nexus URL")
	flags.pathPrefix = cmd.Flags().String("path-prefix", "provision", "Nexus API endpoint")
	flags.nexusUsername = cmd.Flags().String("nexus-username", os.Getenv("NEXUS_USERNAME"), "Nexus Username")
	flags.nexusPassword = cmd.Flags().String("nexus-password", os.Getenv("NEXUS_PASSWORD"), "Nexus Password")
	flags.gitlabURL = cmd.Flags().String("gitlab-url", "", "URL of GitLab")
	flags.gitlabURLPeer = cmd.Flags().String("gitlab-url-peer", "", "URL of GitLab DR site")
	flags.rolePrefix = cmd.Flags().String("role-prefix", "RealmGroup:/", "Role prefix") // Keycloak plugin uses a role prefix, LDAP not
	flags.systemAdminGroup = cmd.Flags().String("system-admin-group", "", "Group representing the system administrators")
	flags.mavenSnapshotCleanupPolicy = cmd.Flags().String("maven-snapshot-cleanup-policy", "", "Cleanup Policy for Maven snapshot repos")
	flags.useExistingRobotPassword = cmd.Flags().Bool("use-existing-robot-password", false, "Use existing password for Nexus robot account")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("nexus-url")
	cmd.MarkFlagRequired("gitlab-url")
	cmd.MarkFlagRequired("publicRoleName")

	return cmd
}

func configureTenant(cmd *cobra.Command, flags *flags) error {

	nexusClient, err := nexus.NewNexusClient(*flags.nexusURL, *flags.nexusUsername, *flags.nexusPassword, *flags.pathPrefix)
	if err != nil {
		return err
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	if !customerConfig.Nexus.UseMaven && !customerConfig.Nexus.UseNpm && !customerConfig.Nexus.UseNuget {
		log.Printf("No Nexus config for customer %s", customerConfig.Name)
		return nil
	}

	if customerConfig.Nexus.UseMaven {
		log.Printf("Configure Maven repo's for %s", customerConfig.Name)

		strictContentTypeValidation := true
		if customerConfig.Nexus.MavenStrictContentTypeValidation == false {
			strictContentTypeValidation = customerConfig.Nexus.MavenStrictContentTypeValidation
		}
		log.Printf("strict content type validation set to %t", strictContentTypeValidation)

		if err := nexusClient.ManageHostedMavenRepository(customerConfig.Name+"-snapshots", "Snapshot", *flags.mavenSnapshotCleanupPolicy, strictContentTypeValidation); err != nil {
			return err
		}
		if err := nexusClient.ManageHostedMavenRepository(customerConfig.Name+"-releases", "Release", "", strictContentTypeValidation); err != nil {
			return err
		}
		if len(customerConfig.Nexus.M2ProxyRepositories) > 0 {
			for _, proxyRepository := range customerConfig.Nexus.M2ProxyRepositories {
				if err := nexusClient.CreateProxyMavenRepository(customerConfig.Name, proxyRepository.Name, proxyRepository.Url, "Release", "", strictContentTypeValidation); err != nil {
					return err
				}
			}
		}
	}

	if customerConfig.Nexus.UseNpm {
		log.Printf("Configure Npm repo's for %s", customerConfig.Name)

		strictContentTypeValidation := true
		if customerConfig.Nexus.NpmStrictContentTypeValidation == false {
			strictContentTypeValidation = customerConfig.Nexus.NpmStrictContentTypeValidation
		}
		log.Printf("strict content type validation set to %t", strictContentTypeValidation)

		// Create general proxy for npm registry 'registry.npmjs.org' if it is not already there
		registryNpmjsOrgName := "registry.npmjs.org"
		registryNpmjsOrgUrl := "https://registry.npmjs.org:443"
		if err := nexusClient.CreateProxyNpmRepository(registryNpmjsOrgName, registryNpmjsOrgUrl, strictContentTypeValidation); err != nil {
			return err
		}

		// + hosted npm repo
		if err := nexusClient.CreateHostedNpmRepository(customerConfig.Name+"-npm", strictContentTypeValidation); err != nil {
			return err
		}

		// + group npm repo containing the hosted one and the general 'registry.npmjs.org'
		groupRepos := []string{customerConfig.Name + "-npm", registryNpmjsOrgName}
		if err := nexusClient.CreateGroupNpmRepository(customerConfig.Name+"-npm-group", groupRepos, strictContentTypeValidation); err != nil {
			return err
		}
	}

	if customerConfig.Nexus.UseNuget {
		log.Printf("Configure Nuget repo's for %s", customerConfig.Name)

		strictContentTypeValidation := true
		if customerConfig.Nexus.NugetStrictContentTypeValidation == false {
			strictContentTypeValidation = customerConfig.Nexus.NugetStrictContentTypeValidation
		}
		log.Printf("strict content type validation set to %t", strictContentTypeValidation)

		// Create general proxy for nuget registry 'api.nuget.org' if it is not already there
		registryNugetjsOrgName := "api.nuget.org"
		registryNugetjsOrgUrl := "https://api.nuget.org/v3/index.json"
		if err := nexusClient.CreateProxyNugetRepository(registryNugetjsOrgName, registryNugetjsOrgUrl, strictContentTypeValidation); err != nil {
			return err
		}

		// + hosted nuget repo
		if err := nexusClient.CreateHostedNugetRepository(customerConfig.Name+"-nuget", strictContentTypeValidation); err != nil {
			return err
		}

		// + group nuget repo containing the hosted one and the general 'api.nuget.org'
		groupRepos := []string{customerConfig.Name + "-nuget", registryNugetjsOrgName}
		if err := nexusClient.CreateGroupNugetRepository(customerConfig.Name+"-nuget-group", groupRepos, strictContentTypeValidation); err != nil {
			return err
		}
	}

	// create a second set of repo's that will not be added to the public rbac role if both public and private repos are requested
	if customerConfig.Nexus.PublicAndPrivateRepo == true {

		NpmStrictContentTypeValidation := true
		if customerConfig.Nexus.NpmStrictContentTypeValidation == false {
			NpmStrictContentTypeValidation = customerConfig.Nexus.NpmStrictContentTypeValidation
		}
		log.Printf("Npm strict content type validation set to %t", NpmStrictContentTypeValidation)

		NugetStrictContentTypeValidation := true
		if customerConfig.Nexus.NugetStrictContentTypeValidation == false {
			NugetStrictContentTypeValidation = customerConfig.Nexus.NugetStrictContentTypeValidation
		}
		log.Printf("Nuget strict content type validation set to %t", NugetStrictContentTypeValidation)

		MavenStrictContentTypeValidation := true
		if customerConfig.Nexus.MavenStrictContentTypeValidation == false {
			MavenStrictContentTypeValidation = customerConfig.Nexus.MavenStrictContentTypeValidation
		}
		log.Printf("Maven strict content type validation set to %t", MavenStrictContentTypeValidation)

		if err := nexusClient.CreateHostedNpmRepository(customerConfig.Name+"-npm-private", NpmStrictContentTypeValidation); err != nil {
			return err
		}

		if err := nexusClient.CreateHostedNugetRepository(customerConfig.Name+"-nuget-private", NugetStrictContentTypeValidation); err != nil {
			return err
		}

		if err := nexusClient.ManageHostedMavenRepository(customerConfig.Name+"-releases-private", "Release", "", MavenStrictContentTypeValidation); err != nil {
			return err
		}

		if err := nexusClient.ManageHostedMavenRepository(customerConfig.Name+"-snapshots-private", "Snapshot", *flags.mavenSnapshotCleanupPolicy, MavenStrictContentTypeValidation); err != nil {
			return err
		}
	}

	// check existence of public role
	if err := nexusClient.CreatePublicRole(*flags.publicRoleName); err != nil {
		return err
	}

	if err := createDeployerRole(nexusClient, *flags.publicRoleName, customerConfig); err != nil {
		return err
	}

	if *flags.useExistingRobotPassword {
		if err := createDeployerUserWithExistingPassword(nexusClient, *flags.gitlabURL, *flags.publicRoleName, customerConfig); err != nil {
			return err
		}
	} else {
		if err := createDeployerUserWithNewPassword(nexusClient, *flags.gitlabURL, *flags.gitlabURLPeer, *flags.publicRoleName, customerConfig); err != nil {
			return err
		}
	}

	// Rolebinding style authorization
	err = configureRoleBindings(nexusClient, customerConfig, *flags.systemAdminGroup, *flags.rolePrefix)

	return err
}

// Example rolebinding config is provided below.
// nexus:
//
//	roles:
//	- name: admin
//	  groups:
//	  - sample-developer
func configureRoleBindings(nexusClient *nexus.Client, customerConfig *config.Customer, systemAdminGroup string, rolePrefix string) error {

	for _, rolebinding := range customerConfig.Nexus.Rolebindings {
		if rolebinding.Name == "admin" {
			for _, group := range rolebinding.Groups {
				if group != systemAdminGroup {
					log.Printf("Ensure group %q has role %q on %q repos in Nexus", group, rolebinding.Name, customerConfig.Name)
					if err := nexusClient.CreateAdminRole(*customerConfig, group, rolePrefix); err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}
