module gitlab.com/logius/cloud-native-overheid/tools/nexus-cli

go 1.22

toolchain go1.22.2

require (
	github.com/hashicorp/go-retryablehttp v0.7.5
	github.com/pkg/errors v0.9.1
	github.com/sethvargo/go-password v0.2.0
	github.com/spf13/cobra v1.8.0
	github.com/xanzy/go-gitlab v0.102.0
	gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli v0.0.0-20240409094811-e7e7cf451086
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/oauth2 v0.19.0 // indirect
	golang.org/x/time v0.5.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
)
