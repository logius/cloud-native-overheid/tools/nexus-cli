package config

import (
	"io/ioutil"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

// GroupConfig spec
type GroupConfig struct {
	Name    string   `validate:"required" yaml:"name"`
	Admin   bool     `yaml:"admin"`
	Members []string `yaml:"members"`
}

// GitLabGroup specifies a group in GitLab
type GitLabGroup struct {
	Name string `validate:"required"`
}

// GitLab configuration
type GitLab struct {
	Groups []GitLabGroup `validate:"required,dive"`
}

// Rolebinding defines the binding between role and zero or more groups
type Rolebinding struct {
	Name   string
	Groups []string `validate:"required" yaml:"groups,omitempty"`
}

type ProxyRepository struct {
	Name string `validate:"required"`
	Url  string `validate:"required"`
}

type Nexus struct {
	UseMaven             				bool `yaml:"use_maven"`
	MavenStrictContentTypeValidation 	bool `yaml:"maven_sctv"`
	UseNpm               				bool `yaml:"use_npm"`
	NpmStrictContentTypeValidation 		bool `yaml:"npm_sctv"`
	UseNuget               				bool `yaml:"use_nuget"`
	NugetStrictContentTypeValidation 	bool `yaml:"nuget_sctv"`
	PrivateRepo          				bool `yaml:"private_repos"`
	PublicAndPrivateRepo 				bool `yaml:"public_and_private_repos"`
	// authorization with rolebindings follows same model as other tools, see e.g. harbor and rancher
	Rolebindings        				[]Rolebinding     `yaml:"roles"`
	M2ProxyRepositories 				[]ProxyRepository `yaml:"m2_proxy_repositories"`
}

// Customer configuration
type Customer struct {
	Name   string
	Groups []GroupConfig `validate:"required,dive"`
	GitLab GitLab        `validate:"required"`
	Nexus  Nexus         `validate:"required"`
}

// Config contains customer configuration
type Config struct {
	Customer Customer `validate:"required"`
}

// LoadConfigfile loads the customer configuration from file
func LoadConfigfile(configFile string) (*Customer, error) {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}
	return NewConfig(string(yamlFile))
}

// NewConfig gets the customer configuration
func NewConfig(configData string) (*Customer, error) {

	var cfg Config
	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	return &cfg.Customer, nil
}
