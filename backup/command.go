package backup

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"github.com/hashicorp/go-retryablehttp"
	"github.com/spf13/cobra"
	nexus "gitlab.com/logius/cloud-native-overheid/tools/nexus-cli/client"
)

type flags struct {
	nexusURL          *string
	pathPrefix        *string
	secondaryNexusURL *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := backupRestore(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "backup-restore",
		Short: "Backup and Restore repositories",
		Long:  "This command peforms backup and restore of repositories in Nexus.",
	}

	flags.nexusURL = cmd.Flags().String("nexus-url", "", "Nexus URL")
	flags.pathPrefix = cmd.Flags().String("path-prefix", "provision", "Nexus API endpoint")
	flags.secondaryNexusURL = cmd.Flags().String("secondary-nexus-url", "", "Secondary Nexus URL")

	cmd.MarkFlagRequired("nexus-url")
	cmd.MarkFlagRequired("secondary-nexus-url")

	return cmd
}

func backupRestore(cmd *cobra.Command, flags *flags) error {

	nexusClient, err := nexus.NewNexusClient(*flags.nexusURL, os.Getenv("NEXUS_USERNAME"), os.Getenv("NEXUS_PASSWORD"), *flags.pathPrefix)
	if err != nil {
		return err
	}
	nexusClient.PathPrefix = "service" // api ingress is used with normal service prefix

	secondaryNexusClient, err := nexus.NewNexusClient(*flags.secondaryNexusURL, os.Getenv("SECONDARY_NEXUS_USERNAME"), os.Getenv("SECONDARY_NEXUS_PASSWORD"), *flags.pathPrefix)
	if err != nil {
		return err
	}
	secondaryNexusClient.PathPrefix = "service" // api ingress is used with normal service prefix

	if err := backupRestoreMavenRepositories(nexusClient, secondaryNexusClient); err != nil {
		return err
	}

	return nil
}

func backupRestoreMavenRepositories(nexusClient *nexus.Client, nexusTargetClient *nexus.Client) error {
	repositories, err := nexusClient.GetHostedMavenReleaseRepositories()
	if err != nil {
		return err
	}

	for _, repository := range repositories {
		assets, err := nexusClient.GetMavenAssets(repository)
		if err != nil {
			return err
		}
		for _, asset := range assets {
			log.Printf("%v", asset.Path)
			if err := replicateAsset(nexusClient, nexusTargetClient, &asset); err != nil {
				return err
			}
		}
	}

	return nil
}

func replicateAsset(nexusSourceClient *nexus.Client, nexusTargetClient *nexus.Client, asset *nexus.Asset) error {

	targetUrl := fmt.Sprintf("%v/repository/%v/%v", nexusTargetClient.NexusURL, asset.Repository, asset.Path)

	existing, err := fileExists(nexusTargetClient, targetUrl)
	if err != nil {
		return err
	}
	if existing {
		return nil
	}

	req, err := http.NewRequest(http.MethodGet, asset.DownloadURL, nil)
	if err != nil {
		return err
	}
	req.SetBasicAuth(nexusSourceClient.Username, nexusSourceClient.Password)

	retryClient := retryablehttp.NewClient()
	retryClient.Logger = nil
	retryClient.RetryMax = 10
	standardClient := retryClient.StandardClient()

	resp, err := standardClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusNotFound {
		log.Printf("Warning: content not found: %s", asset.DownloadURL)
		return nil // ignore this error
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", resp.Status)
	}

	return newfileUpload(nexusTargetClient, asset, targetUrl, resp.Body)
}

func fileExists(nexusClient *nexus.Client, url string) (bool, error) {
	request, err := http.NewRequest(http.MethodHead, url, nil)
	if err != nil {
		return false, err
	}
	request.SetBasicAuth(nexusClient.Username, nexusClient.Password)
	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		return false, err
	}
	return resp.StatusCode == 200, nil
}

func newfileUpload(nexusClient *nexus.Client, asset *nexus.Asset, url string, responseData io.ReadCloser) error {

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(asset.Path))
	if err != nil {
		return err
	}
	_, err = io.Copy(part, responseData)
	if err != nil {
		return err
	}

	err = writer.Close()
	if err != nil {
		return err
	}

	request, err := http.NewRequest(http.MethodPut, url, body)
	request.SetBasicAuth(nexusClient.Username, nexusClient.Password)
	request.Header.Set("Content-Type", writer.FormDataContentType())

	if err != nil {
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		return err
	} else {
		// TODO should we fail on 404 ?
		fmt.Printf("Upload result %v: %v\n", url, resp.StatusCode)
	}

	return nil
}
